---
layout: markdown_page
title: "BU.1.02 - Resilience Testing Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# BU.1.02 - Resilience Testing

## Control Statement

GitLab performs backup restoration and/or failover tests quarterly to confirm the reliability and integrity of system backups and/or recovery operations.

## Context

By validating system backups/recovery operations in the event of an actual disaster or other disruption to service; we will have greater proficiency in restoring service to customers.

## Scope

This control applies to all systems within our production environment. The production environment includes all endpoints and cloud assets used in hosting GitLab.com and its subdomains. This may include third-party systems that support the business of GitLab.com.

## Ownership

* Control Owner: `Infrastructure Team`
* Process owner(s): 
    * Infrastructure Team

## Guidance

This guidance is a two-parter, provide evidence demonstrating:
* Documentation of incident response plan, including:
   * Roles and responsibilities
   * Specific procedures
   * Business recovery and continuity procedures
   * Backup processes
   * Legal requirements for reporting compromises
   * Coverage and responses for all critical systems
   * Reference/inclusion of incident response procedures from the payment brands
* Sample of previously reported incidents documentation

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Resilience Testing control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/779).

Examples of evidence an auditor might request to satisfy this control:

* A copy of GitLab's backup, disaster recovery, and incident response processes
* Documentation showing the testing of backup and disaster recovery procedure happens, at minimum, on a quarterly basis

## Framework Mapping

* ISO
  * A.12.3.1
* SOC
  * A1.2
* PCI
  * 12.10.1
