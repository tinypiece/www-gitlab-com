---
layout: markdown_page
title: "SO.2.03 - Periodic Review of Physical Access Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SO.2.03 - Periodic Review of Physical Access

## Control Statement

GitLab performs physical access account reviews quarterly; corrective action is take where applicable.

## Context

This control refers to physical access of any GitLab facilities.

## Scope

This control is not applicable since GitLab has no facilities.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Periodic Review of Physical Access control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/894).

## Framework Mapping

* ISO
  * A.11.1.2
* SOC2 CC
  * CC6.4
* PCI
  * 9.5
