---
layout: markdown_page
title: "UrbanCode Deploy"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary
IBM UrbanCode Deploy is a tool for automating application deployments through your environments. It is designed to facilitate rapid feedback and continuous delivery in agile development while providing the audit trails, versioning and approvals needed in production.

Although IBM sells all the UrbanCode products under its brand, HCL also sells some of the same tools under its own brand. 

On January 30, 2018, [IBM Announced it's partnership with HCL](https://developer.ibm.com/urbancode/2018/01/30/partnering-hcl-growth-innovation/) for continued growth and innovation.



## Resources
* [HCL UrbanCode Deploy](https://www.hcltech.com/software/urbancode-deploy)
* [HCL UrbanCode Deploy Video](https://youtu.be/TJ_96qLAgsg)
* [HCL UrbanCode Velocity](https://www.hcltech.com/software/urbancode-velocity)

## Related Tools

* [Electric Cloud](https://about.gitlab.com/devops-tools/electric-flow-vs-gitlab.html)

* [CodeFresh](https://about.gitlab.com/devops-tools/codefresh-vs-gitlab.html)

* [Xebia Labs Deploy](https://about.gitlab.com/devops-tools/xebialabs-vs-gitlab.html)


